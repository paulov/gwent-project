#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include <iostream>
#include "Menu.h"
#include <stack>


//Codigo de Compilacao
//g++ -std=c++0x main.cpp Menu.cpp -o drive -lsfml-audio -lsfml-graphics -lsfml-window -lsfml-system
//Exucucao   
//./drive


/*
* ****************************Projeto Gwent
* ****************************Projeto Desenvolvido por Paulo Victor Sousa 
* ****************************Materia EDB1/LP1
*
*/

struct Card{

	sf::Sprite sprite;
	sf::Texture texture;
	int position;
	int value;
	bool spy;
};


class player{
public:
	Card card[20]; //cartas do deck
	//int catapultRow[5];
	//int archersRow[5];
	//int meeleesRow[5];
	int points;//pontos totais do player
	int passed;//variavel que indica caso o player tenha ou nao passado d vez
	int victory;//variavel que indica o player que sera vitorioso
    Card tmp;//Variavel auxiliar que sera usada para fazer o embaralhamento
	std::stack<Card> deck;//deck do player
	player(){
		points = 0;
		passed = 0;
		victory =0;

	 	
	 	tmp.position = 1;
		tmp.value = 10;

		if(!card[0].texture.loadFromFile("image/img5.jpeg"));
		if(!card[1].texture.loadFromFile("image/img6.jpeg"));
		if(!card[2].texture.loadFromFile("image/img7.jpeg"));
		if(!card[3].texture.loadFromFile("image/img8.jpeg"));
		if(!card[4].texture.loadFromFile("image/img10.jpeg"));
		if(!card[5].texture.loadFromFile("image/img13.jpeg"));
		if(!card[6].texture.loadFromFile("image/img14.jpeg"));
		if(!card[7].texture.loadFromFile("image/img15.jpeg"));
		if(!card[8].texture.loadFromFile("image/img16.jpeg"));
		if(!card[9].texture.loadFromFile("image/img21.jpeg"));
		if(!card[10].texture.loadFromFile("image/img7.jpeg"));
		if(!card[11].texture.loadFromFile("image/img10.jpeg"));
		if(!card[12].texture.loadFromFile("image/img13.jpeg"));
		if(!card[13].texture.loadFromFile("image/img13.jpeg"));
		if(!card[14].texture.loadFromFile("image/img17.jpeg"));
		if(!card[15].texture.loadFromFile("image/img17.jpeg"));
		if(!card[16].texture.loadFromFile("image/img18.jpeg"));
		if(!card[17].texture.loadFromFile("image/img20.jpeg"));
		if(!card[18].texture.loadFromFile("image/img20.jpeg"));
		if(!card[19].texture.loadFromFile("image/img20.jpeg"));



		for(int i=0;i<10;i++){
			card[i].sprite.setPosition(90*i,600); //Posiçao das cartas da mao do jogador
			
		}

		sf::Vector2f scale;
		for(int i=0;i<20;i++){
			card[i].sprite.setTexture(card[i].texture);
		}

		for(int i=0;i<20;i++){
			scale = card[i].sprite.getScale();
    		card[i].sprite.setScale(scale.x/9,scale.y/17);//escalonamento das cartas 
		}

		//setando fileiras das cartas
		card[0].position = 1;
		card[1].position = 1;
		card[2].position = 3;
		card[3].position = 2;
		card[4].position = 2;
		card[5].position = 1;
		card[6].position = 2;
		card[7].position = 1;
		card[8].position = 1;
		card[9].position = 1;
		card[10].position = 3;
		card[11].position = 3;
		card[12].position = 1;
		card[13].position = 1;
		card[14].position = 3;
		card[15].position = 3;
		card[16].position = 3;
		card[17].position = 2;
		card[18].position = 2;
		card[19].position = 2;

		//setando valores das cartas
		card[0].value = 10;
		card[1].value = 10;
		card[2].value = 5;
		card[3].value = 10;
		card[4].value = 6;
		card[5].value = 4;
		card[6].value = 5;
		card[7].value = 5;
		card[8].value = 10;
		card[9].value = 4;
		card[10].value = 5;
		card[11].value = 6;
		card[12].value = 4;
		card[13].value = 4;
		card[14].value = 6;
		card[15].value = 6;
		card[16].value = 1;
		card[17].value = 5;
		card[18].value = 5;
		card[19].value = 5;

		//indicando quais cartas sao de puxar cartas
		for(int i=0;i<20;i++){
			card[i].spy = 0;
		}
		card[6].spy = 1;
		card[16].spy = 1;
		
		int remaining=20;
	 	while (remaining>0){//Embaralhamento das cartas
        	int k = rand() % remaining;
 
        	//tmp = card[k];
        	
	        tmp.texture = card[k].texture;
	        tmp.value = card[k].value;
	        tmp.position = card[k].position;
	        tmp.spy = card[k].spy;
			
			//card[k] = card[remaining-1];
	        
	        card[k].texture = card[remaining-1].texture;
	        card[k].value = card[remaining-1].value;
	        card[k].position = card[remaining-1].position;
	        card[k].spy = card[remaining-1].spy;
			
	        //card[remaining-1] = tmp;
	        
	        card[remaining-1].texture = tmp.texture;
	        card[remaining-1].value = tmp.value;
	        card[remaining-1].position = tmp.position;
	        card[remaining-1].spy = tmp.spy;
	        
	 
	        remaining--;
	   	}

	   	for(int i=10;i<20;i++){//Jogando no deck as cartas que nao foram embaralhadas direto para a mao
	   		deck.push(card[i]);
	   	}

	}
	/*void draw(sf::RenderWindow &window){
		
		for(int i =0;i<10;i++){
			window.draw(card[i].sprite);
		}

	}*/
};



using namespace std;

enum screenstate{ //Telas do jogo 
	first,
	second,
};


int main()
{
	sf::RenderWindow window(sf::VideoMode(900, 700), "SFML WORKING!");

	Menu menu(window.getSize().x, window.getSize().y);

	player PRAYER[2];//PLayers do jogo

	int target=0;//indice do vetor de cards da mao

	int status = first;// Pagina do jogo aberta

	//int passed=0;


	sf::Texture tox; //textura do tabuleiro
	sf::Sprite sprote;

	sf::Sprite sprite1;// Rei 1 ( so para indicar qual o player do deck)
	sf::Texture image1;

	sf::Sprite sprite2;// Rei 2 ( so para indicar qual o player do deck)
	sf::Texture image2;

	sf::Music music;

	if(!music.openFromFile("audio.ogg")){
		cout<<"Error loading the music"<<endl;
	}

	if(!image1.loadFromFile("img30.jpeg")){
		std::cout<< "Error loading first card"<< std::endl;
	}

	if(!tox.loadFromFile("board.jpg")){
		std::cout << "Error loading Vertex texture :(" << std::endl;
	}

	if(!image2.loadFromFile("img31.jpeg")){
		std::cout<< "Error loading first card"<< std::endl;
	}

    sprite2.setTexture(image2);
    sprite1.setTexture(image1);

    sf::Vector2f scale = sprite2.getScale();
    sprite2.setScale(scale.x/5,scale.y/4);// Escalonando o Rei 1
    sprite2.setPosition(600,100);

    sprite1.setScale(scale.x/5,scale.y/4);// Escalonando o Rei 2
    sprite1.setPosition(600,100);

    sprote.setTexture(tox);// textura do tabuleiro
    

    sf::RectangleShape rect(sf::Vector2f(90,5));// Retangulo seletor de card
    rect.setPosition(0,600);//posicao inicial do retangulo
    rect.setFillColor(sf::Color(255,255,0));//cor amarela
    
    sf::Vector2f position(90,0);//posicao a se adcionar no retangulo
    sf::Vector2f maxPosition(900,0);//posicao maxima do retangulo
    sf::Vector2f minPosition(0,0);//posicao minima do retangulo

    sf::Vector2f xRange(0,300);//posicao a incrementar no retangulo

    int turn = 1;// turno do determinado player (1 ou 2)
 	int Round = 1;
    int amount=10;// quantidade de cartas utilizadas no momento ( mais caso haja cards puxados)
    Card temp;// variavel auxiliar criada para fazer a troca de posicao com a carta da mao e a carta recem puxada
    int verifyer = 0;// variavel para confirmacao de se o card eh ou nao de puxar cards

	while (window.isOpen())
	{
		sf::Event event;// evento inicial
		
		while (window.pollEvent(event))
		{

			switch (event.type)
			{
			case sf::Event::KeyPressed:
				switch (event.key.code)
				if(status == first){ // Primeiro status da tela (menu principal)
				{
				
				case sf::Keyboard::Up:
					menu.MoveUp();
					break;

				case sf::Keyboard::Down:
					menu.MoveDown();
					break;

				case sf::Keyboard::Return:
					switch (menu.GetPressedItem())
					{
					case 0:
						std::cout << "Play button has been pressed" << std::endl;
						status = second; // pagina do jogo
						
						break;
					case 1:
						std::cout << "Option button has been pressed" << std::endl;//opcao d tocar musica
						music.play();
						break;
					case 2:
						window.close();
						break;
					}

					break;
				}
				}

				if(status == second){
					if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){//mudando a posicao do retangulo seletor d cartas p direita

                		rect.setPosition(rect.getPosition() + position);
                		target++;//incremento indice da carta da mao
                		if(rect.getPosition().x >= maxPosition.x ){//caso esteja numa posicao maior q a maxima, volta pro começo
                			rect.setPosition(0,600);
                			target=0;
                		}
            		}

            		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){//mudando a posicao do retangulo seletor d cartas p esquerda
                		rect.setPosition(rect.getPosition() - position);
                		target--;//decremento indice da carta da mao
                		if(rect.getPosition().x < minPosition.x){// caso esteja numa posicao menor q a minima, volta pro maximo
                			rect.setPosition(810,600);
                			target=9;
                		}
            		}

				}



				break;
				case sf::Event::Closed:
					window.close();
					break;

			}

		window.clear(sf::Color(0,240,255));//cor de fundo

		if(status == first){
			//window.draw(sprite);
			menu.draw(window);//impressao do menu principal
			
		}
		if(status == second){
			
			window.draw(sprote);//desenho do tabuleiro
			if(turn == 1){
				window.draw(sprite2);

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::P)){//tecla de parar os movimentos do jogador 1
            	std::cout<< "PLayer " << 1 << " PASSED "<< std::endl;
            	PRAYER[0].passed = 1;
            	std::cout<< " pressionou o P"<<  std::endl;

            	if(PRAYER[1].passed == 1){//  Caso o outro player tbm tenha parado os movimentos
            		std::cout<< "  Game Over"<< std::endl;
            		if(PRAYER[0].points > PRAYER[1].points){
            			std::cout<< std::endl;
            			PRAYER[0].victory++;//confirmando a vitoria
            			std::cout<< "VITORIA DO PLAYER 1"<<std::endl;
            			window.close();
            		}
            		if(PRAYER[0].points < PRAYER[1].points){
            			std::cout<< std::endl;
            			PRAYER[1].victory++;//confirmando a vitoria
            			std::cout << "VITORIA DO PLAYER 2"<<std::endl;
            			window.close();

            		}
            		if(PRAYER[1].points == PRAYER[0].points && PRAYER[1].points != 0){// Nao aceita caso ambos nao tenham jogado nd
            			std::cout<< std::endl;
            			PRAYER[0].victory++;//confirmando o empate
            			PRAYER[1].victory++;//confirmando o empate
            			std::cout << "EMPATE " << std::endl;
            			window.close();
            		}
            		if(PRAYER[1].points == 0 && PRAYER[0].points == 0){
            			std::cout<< std::endl;
            			std::cout<< "Empate forçado pelos players"<<std::endl;
            		}
            	}
            	else{
            		turn = 2;//passando o turno pro player 2
            	}

            }

            if(PRAYER[0].passed == 0){//caso o jogador nao tenha parado seus movimentos
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)){//tecla de jogada do player 1
            		window.draw(sprite1);
            		PRAYER[0].points = PRAYER[0].points + PRAYER[0].card[target].value;//soma do ponto conforme o card
         			std::cout<<" Player 1 points "<< PRAYER[0].points << std::endl << std::endl;
         			std::cout<<" Turno do player "<< 2 << std::endl;


         			//setando as possiveis posicoes nas linhas dos cards
	            	if(PRAYER[0].card[target].position == 1){
	            		PRAYER[0].card[target].sprite.setPosition( xRange.x,300);
	            		xRange.x = xRange.x + 25;
	            	}
	            	if(PRAYER[0].card[target].position == 2){
	            		PRAYER[0].card[target].sprite.setPosition( xRange.x,400);
	            		xRange.x = xRange.x + 25;
	            	}
	            	if(PRAYER[0].card[target].position == 3){
	            		PRAYER[0].card[target].sprite.setPosition( xRange.x,500);
	            		xRange.x = xRange.x + 25;
	            	}

	            	
	            	verifyer = PRAYER[0].card[target].spy;// confirmacao da verificacao de se o card eh um de puxar cards
	            	if(PRAYER[0].card[target].spy = 1){ //caso o card eh d puxar cards
	            		if(PRAYER[0].card[target].spy == verifyer){// confirmacao da verificacao de se o card eh um de puxar cards
		            		//std::cout<<PRAYER[0].card[target].spy<<endl;
		            		//std::cout<<verifyer<<endl;
			            	
	         				if(!PRAYER[0].deck.empty()){ //Tratamento para nao causar segmentation falt com a pilha
		         				//PRAYER[0].card[amount].texture = PRAYER[0].deck.top().texture;
		         				//PRAYER[0].card[amount].position = PRAYER[0].deck.top().position;
		         				//PRAYER[0].card[amount].value = PRAYER[0].deck.top().value;
		         				//PRAYER[0].card[amount].spy = PRAYER[0].deck.top().spy;
		         				PRAYER[0].card[amount] = PRAYER[0].deck.top();//todos esses acima se resumem nessa linha

		         				//setando a posicao da jogada
		         				if(PRAYER[0].card[amount].position == 1){
				            		PRAYER[0].card[amount].sprite.setPosition( xRange.x,300);
				            		xRange.x = xRange.x + 25;
				            	}

				            	if(PRAYER[0].card[amount].position == 2){
				            		PRAYER[0].card[amount].sprite.setPosition( xRange.x,400);
				            		xRange.x = xRange.x + 25;
				            	}

				            	if(PRAYER[0].card[amount].position == 3){
				            		PRAYER[0].card[amount].sprite.setPosition( xRange.x,500);
				            		xRange.x = xRange.x + 25;
				            	}

				            	// esses ate o fim desse comentario servem para trocar a localizacao do card recem puxado com a localizacao do card que foi jogado para puxar
				            	PRAYER[0].card[target].sprite.setPosition(target*90,600);

				            	temp.texture = PRAYER[0].card[amount].texture;
		         				temp.position = PRAYER[0].card[amount].position;
		         				temp.value = PRAYER[0].card[amount].value;
		         				temp.spy = PRAYER[0].card[amount].spy ;

		         				PRAYER[0].card[amount].texture = PRAYER[0].card[target].texture;
		         				PRAYER[0].card[amount].position = PRAYER[0].card[target].position;
		         				PRAYER[0].card[amount].value = PRAYER[0].card[target].value;
		         				PRAYER[0].card[amount].spy = PRAYER[0].card[target].spy;

		         				PRAYER[0].card[target].texture = temp.texture;
		         				PRAYER[0].card[target].position = temp.position;
		         				PRAYER[0].card[target].value = temp.value;
		         				PRAYER[0].card[target].spy = temp.spy;



		         				PRAYER[0].deck.pop();
		         				std::cout<<"A card has been pulled "<<endl;
		         				amount++;
		         				//fim do comentario
	         				}
         				}
         			}

	            	if(PRAYER[1].passed == 0){
	            		turn = 2;//turno passado para o player 2
	            	}
	       
            }
        	}

        	if(Round == 1){
				//window.draw(sprite2);
				//PRAYER[0].draw(window);
				for(int i =0;i<amount;i++){
					window.draw(PRAYER[0].card[i].sprite);//impressao das cartas 
				}
				window.draw(rect);//impressao do retangulo seletor
			}

		}
			if(turn == 2){
				window.draw(sprite1);

				/*if(sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace)){

            		sprite2.setPosition(100,200);

            	}*/
            		///Devido a pressa que tenho que enviar antes que de meia noite, esses comentarios ficarao ate este ponto 
            		///e O CODIGO DEFINITIVO E MAIS ORGANIZADO SERA ENTREGUE NA TERÇA FEIRA 8/12 NA AVALIAÇAO DO PROJETO
            		

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::O)){
            	std::cout<< "PLayer " << 2 << " PASSED "<< std::endl;

            	PRAYER[1].passed = 1;

            	std::cout<< " pressionou o O"<<  std::endl;

            	if(PRAYER[0].passed == 1){
            		std::cout<< " GameOver" << std::endl;
            		if(PRAYER[1].points < PRAYER[0].points){
            			std::cout<< std::endl;
            			PRAYER[0].victory++;
            			std::cout<< "VITORIA DO PLAYER 1"<<std::endl;

            			window.close();
            		}

            		if(PRAYER[1].points > PRAYER[0].points){
            			std::cout<< std::endl;
            			PRAYER[1].victory++;
            			std::cout << "VITORIA DO PLAYER 2"<<std::endl;

            			window.close();
            		}

            		if(PRAYER[1].points == PRAYER[0].points && PRAYER[1].points != 0){
            			std::cout<< std::endl;
            			PRAYER[0].victory++;
            			PRAYER[1].victory++;
            			std::cout << "EMPATE " << std::endl;
            			window.close();
            		}
            	}
            	else{
            		turn = 1;
            	}
            	//std::cout<< "Turno " << turn << std::endl;

            }

            if(PRAYER[1].passed == 0){
	            if(sf::Keyboard::isKeyPressed(sf::	Keyboard::S)){
	            	PRAYER[1].points = PRAYER[1].points + PRAYER[1].card[target].value;
	      			std::cout<<" Player 2 points "<< PRAYER[1].points << std::endl << std::endl;
	      			std::cout<<" Turno do player "<< 1 << std::endl;

	            	if(PRAYER[1].card[target].position == 1){
	            		PRAYER[1].card[target].sprite.setPosition( xRange.x,200);
	            		xRange.x = xRange.x + 25;
	            	}

	            	if(PRAYER[1].card[target].position == 2){
	            		PRAYER[1].card[target].sprite.setPosition( xRange.x,100);
	            		xRange.x = xRange.x + 25;
	            	}

	            	if(PRAYER[1].card[target].position == 3){
	            		PRAYER[1].card[target].sprite.setPosition( xRange.x,0);
	            		xRange.x = xRange.x + 25;
	            	}


	            	verifyer = PRAYER[1].card[target].spy;
		            	if(PRAYER[1].card[target].spy = 1){
		            		if(PRAYER[1].card[target].spy == verifyer){
			            		//std::cout<<PRAYER[1].card[target].spy<<endl;
			            		//std::cout<<verifyer<<endl;
				            	
		         				if(!PRAYER[1].deck.empty()){ //Tratamento para nao causar segmentation falt com a pilha
			         				//PRAYER[1].card[amount].texture = PRAYER[1].deck.top().texture;
			         				//PRAYER[1].card[amount].position = PRAYER[1].deck.top().position;
			         				//PRAYER[1].card[amount].value = PRAYER[1].deck.top().value;
			         				//PRAYER[1].card[amount].spy = PRAYER[1].deck.top().spy;
			         				PRAYER[1].card[amount] = PRAYER[1].deck.top();

			         				if(PRAYER[1].card[amount].position == 1){
					            		PRAYER[1].card[amount].sprite.setPosition( xRange.x,300);
					            		xRange.x = xRange.x + 25;
					            	}
					            	if(PRAYER[1].card[amount].position == 2){
					            		PRAYER[1].card[amount].sprite.setPosition( xRange.x,400);
					            		xRange.x = xRange.x + 25;
					            	}
					            	if(PRAYER[1].card[amount].position == 3){
					            		PRAYER[1].card[amount].sprite.setPosition( xRange.x,500);
					            		xRange.x = xRange.x + 25;
					            	}

					            	PRAYER[1].card[target].sprite.setPosition(target*90,600);

					            	temp.texture = PRAYER[1].card[amount].texture;
			         				temp.position = PRAYER[1].card[amount].position;
			         				temp.value = PRAYER[1].card[amount].value;
			         				temp.spy = PRAYER[1].card[amount].spy ;

			         				PRAYER[1].card[amount].texture = PRAYER[1].card[target].texture;
			         				PRAYER[1].card[amount].position = PRAYER[1].card[target].position;
			         				PRAYER[1].card[amount].value = PRAYER[1].card[target].value;
			         				PRAYER[1].card[amount].spy = PRAYER[1].card[target].spy;

			         				PRAYER[1].card[target].texture = temp.texture;
			         				PRAYER[1].card[target].position = temp.position;
			         				PRAYER[1].card[target].value = temp.value;
			         				PRAYER[1].card[target].spy = temp.spy;



			         				PRAYER[1].deck.pop();
			         				std::cout<<"A card has been pulled "<<endl;
			         				amount++;
		         				}
	         				}
	         			}

	            	if(PRAYER[0].passed == 0){
		            		turn = 1;
		            }

	            }
        	}	

            

        	if(Round == 1){
				//window.draw(sprite2);
				//PRAYER[1].draw(window);
				for(int i =0;i<amount;i++){
					window.draw(PRAYER[1].card[i].sprite);
				}
				window.draw(rect);
				//std::cout<<"Round 1"<<std::endl;
			}

			}

		}
		window.display();

	}

	}

}